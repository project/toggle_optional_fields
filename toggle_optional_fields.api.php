k<?php

/**
 * @file
 * API documentation for Toggle Optional Fields.
 */

/**
 * Allow other modules to provide an array of overridden fields.
 *
 * @param array $overridden_fields
 *   An multi-dimensional array of fields to be overridden. The first key is
 *   the name of the entity type, the second is the bundle name and the third is
 *   the field name.
 *   Each field should return TRUE for it to be always visible or FALSE for it
 *   to be always hidden.
 *   The array is passed by reference so does not need to be returned.
 */
function hook_toggle_optional_fields_overridden_fields_alter(array &$overridden_fields) {
  // Ensure that field_my_field is always shown on article node forms.
  $overridden_fields['node']['article']['field_my_field'] = TRUE;

  // Ensure that field_my_other_field is always hidden on page node forms.
  $overridden_fields['node']['page']['field_my_other_field'] = FALSE;
}
