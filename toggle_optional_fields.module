<?php

/**
 * Implements hook_form_FORM_ID_alter().
 */
function toggle_optional_fields_form_node_type_form_alter(&$form, $form_state) {
  $form['toggle_optional_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Toggle Optional Fields'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
    '#attributes' => array(
      'class' => array('toggle-optional-fields-settings-form'),
    ),
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'toggle_optional_fields') . '/toggle-optional-fields-node-form.js'),
    ),
  );

  $form['toggle_optional_fields']['toggle_optional_fields_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#description' => t('Enable the toggling of optional fields for this content type.'),
    '#default_value' => variable_get('toggle_optional_fields_enabled_' . $form['#node_type']->type, FALSE)
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function toggle_optional_fields_form_node_form_alter(&$form, &$form_state, $form_id) {
  if (variable_get('toggle_optional_fields_enabled_' . $form['#bundle'], FALSE) == FALSE) {
    return;
  }

  module_load_include('inc', 'toggle_optional_fields', 'toggle_optional_fields.forms');

  _toggle_optional_fields_prepare_form($form, $form_state);

  if (empty($form_state['storage']['hide_optional_fields'])) {
    return;
  }

  _toggle_optional_fields_alter_form($form, $form_state);
}

/**
 * Custom submit handler for the node form.
 *
 * @see toggle_optional_fields_form_node_form_alter()
 */
function toggle_optional_fields_submit($form, &$form_state) {
  $form_state['storage']['hide_optional_fields'] = empty($form_state['storage']['hide_optional_fields']);

  $form_state['rebuild'] = TRUE;
}


/**
 * Prepare the form and add the toggle button.
 *
 * @see toggle_optional_fields_form_node_form_alter()
 */
function _toggle_optional_fields_prepare_form(&$form, &$form_state) {
  if (!isset($form_state['storage']['hide_optional_fields'])) {
    $form_state['storage']['hide_optional_fields'] = variable_get('toggle_optional_fields_hide_by_default', TRUE);
  }

  $hidden = $form_state['storage']['hide_optional_fields'];

  $form['toggle_optional_fields'] = array(
    '#type' => 'submit',
    '#value' => t('!action optional fields', array('!action' => $hidden ? 'Show' : 'Hide')),
    '#submit' => array('toggle_optional_fields_submit'),
    '#limit_validation_errors' => array(),
    '#weight' => -100,
  );
}

/**
 * Perform alterations on the node edit form.
 *
 * @see toggle_optional_fields_form_form_alter()
 */
function _toggle_optional_fields_alter_form(&$form, &$form_state) {
  $type = $form['#entity_type'];
  $bundle = $form['#bundle'];

  // Find any optional fields that need to be displayed.
  $overridden_fields = array();
  drupal_alter('toggle_optional_fields_overridden_fields', $overridden_fields);
  if (!empty($overridden_fields[$type][$bundle])) {
    $overridden_fields = $overridden_fields[$type][$bundle];
  }
  $form_state['storage']['toggle_optional_fields_overridden_fields'] = $overridden_fields;

  array_map(function($element_name) use (&$form, $type, $bundle, $overridden_fields) {
    // Only affect fields.
    if (!toggle_optional_fields_element_is_field($element_name)) {
      return;
    }

    $element = &$form[$element_name];

    if (isset($overridden_fields[$element_name])) {
      return $element['#access'] = $overridden_fields[$element_name];
    }

    // If the field is not required, disallow access to hide it.
    if (isset($element[LANGUAGE_NONE][0]['#required'])) {
      return $element['#access'] = !empty($element[LANGUAGE_NONE][0]['#required']);
    }

    if (isset($element[LANGUAGE_NONE]['#required'])) {
      return $element['#access'] = !empty($element[LANGUAGE_NONE]['#required']);
    }
  }, element_children($form));

  $form['#after_build'][] = 'toggle_optional_fields_after_build';
}

function toggle_optional_fields_after_build(&$form, &$form_state) {
  if (empty($form_state['storage']['toggle_optional_fields_overridden_fields'])) {
    return $form;
  }

  foreach ($form_state['storage']['toggle_optional_fields_overridden_fields'] as $field => $value) {
    $form[$field]['#access'] = $value;
  }

  return $form;
}
