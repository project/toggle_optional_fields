(function ($) {

  Drupal.behaviors.toggleOptionalFieldsFieldsetSummaries = {
    attach: function (context) {
      // Provide the summary for the node type form.
      $('fieldset.toggle-optional-fields-settings-form', context).drupalSetSummary(function (context) {
        if (!$('.form-item-toggle-optional-fields-enabled input:checked', context).is(':checked')) {
          return Drupal.t('Disabled');
        }

        return $('.form-item-toggle-optional-fields-enabled input').next('label').text();
      });
    }
  };

})(jQuery);
